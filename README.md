# Axwave Cordova Plugin
A Cordova plugin for managing AxwaveSDK on Android



Simple configuration with custom notification and callback registered

    var app = {
    
        ready: false,
    
        initialize: function () {
            document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
            document.addEventListener("resume", this.onResume.bind(this), false);
        },
    
        onDeviceReady: function () {
            this.onSetup();
            cordova.plugins.AxwaveSDK.setNotificationResources("eliasz", "kubala", "icon"); // change notification title / description / icon. Icon has to be added to res/icon/android/ directory.
            cordova.plugins.AxwaveSDK.registerCallback(this.debug.bind(this), this.error.bind(this));
        },
    
        onResume: function () {
            this.onSetup();
        },
    
    
        onSetup: function () {
            if (this.ready) return;
    
            cordova.plugins.AxwaveSDK.setup(
                    "xxxx-xxxxxxxx", //dev key
                    function (test) {
                        this.ready = true;
    
                        console.log(test)
                    }.bind(this), function (test) {
                        this.ready = false;
                        console.log(test)
                    }.bind(this));
        },
    
        debug: function (msg) {
            console.log("debug: " + msg);
        },
    
        error: function (msg) {
            console.log("error" + msg);
        }
        
    };
    
    app.initialize();


Change notification title / description / icon. Icon has to be added to `res/icon/android/` directory.

    cordova.plugins.AxwaveSDK.setNotificationResources("eliasz", "kubala", "icon");

Enable discovery

    cordova.plugins.AxwaveSDK.enable();

Disable discovery

    cordova.plugins.AxwaveSDK.disable();

Get the status of discovery (String)

    cordova.plugins.AxwaveSDK.isEnabled(function (enabled) {
         //enabled = [true / false]
    });
 
Stop SDK

    cordova.plugins.AxwaveSDK.kill();

If autostart is enabled, SDK will start after device restart

    cordova.plugins.AxwaveSDK.autostart(true);

Android allow to create only once instance for microphone. So if our SDK is working and user want to use for example Google Voice Assistant it's not allowed cause mic is blocked. This algo is getting list of apps which gonna use microphone. If user open one of them, our SDK is releasing microphone. If foreground apps doesn't need mic, we are enabling SDK automatically.
          
    cordova.plugins.AxwaveSDK.foregroundAppsDetection.enable(); //enable this algo
    cordova.plugins.AxwaveSDK.foregroundAppsDetection.disable(); //disable this algo
          
 
Get the status of foregroundAppsDetection algo
 
    cordova.plugins.AxwaveSDK.foregroundAppsDetection.status(function (status) {
       //status = [true / false]
    });