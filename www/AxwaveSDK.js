let AxwaveSDK = function (require, exports, module) {
    let exec = require('cordova/exec');

    let PLUGIN_NAME = 'AxwaveCordovaPlugin';

    function AxwaveSDK() {
    }

    AxwaveSDK.prototype.setup = function (deviceKey, success, failure) {
        exec(success, failure, PLUGIN_NAME, "setup", [deviceKey]);
    };


    AxwaveSDK.prototype.setNotificationResources = function (title, description, image) {
        exec(null, null, PLUGIN_NAME, "setNotificationResources", [title, description, image]);
    };

    AxwaveSDK.prototype.enable = function () {
        exec(null, null, PLUGIN_NAME, "enable", []);
    };

    AxwaveSDK.prototype.disable = function () {
        exec(null, null, PLUGIN_NAME, "disable", []);
    };

    AxwaveSDK.prototype.isEnabled = function (callback) {
        exec(callback, null, PLUGIN_NAME, "isEnabled", []);
    };

    AxwaveSDK.prototype.registerCallback = function (debug, error) {
        exec(debug, error, PLUGIN_NAME, "callback", [])
    };

    AxwaveSDK.prototype.kill = function () {
        exec(null, null, PLUGIN_NAME, "kill", []);
    };

    AxwaveSDK.prototype.autostart = function (booleanValue) {
        exec(null, null, PLUGIN_NAME, "autostart", [booleanValue]);
    };

    AxwaveSDK.prototype.foregroundAppsDetection = Object();

    AxwaveSDK.prototype.foregroundAppsDetection.status = function (callback) {
        exec(callback, null, PLUGIN_NAME, "foregroundStatus", [])
    };

    AxwaveSDK.prototype.foregroundAppsDetection.enable = function () {
        exec(null, null, PLUGIN_NAME, "foregroundEnable", [])
    };

    AxwaveSDK.prototype.foregroundAppsDetection.disable = function () {
        exec(null, null, PLUGIN_NAME, "foregroundStatus", [])
    };


    let instance = new AxwaveSDK();
    module.exports = instance;
};

AxwaveSDK(require, exports, module);

cordova.define("cordova/plugin/AxwaveCordovaPlugin", AxwaveSDK);
