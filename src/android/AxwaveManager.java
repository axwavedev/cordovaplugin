package axwavecordovaplugin;

import android.Manifest;
import android.app.Activity;
import android.app.AppOpsManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;
import com.axwave.sdk.AxwaveSDK;
import com.axwave.sdk.DiscoveryReceiver;
import com.axwave.sdk.FailureType;
import com.axwave.sdk.log.Logger;
import org.apache.cordova.*;
import org.json.JSONArray;
import org.json.JSONException;


public class AxwaveManager extends CordovaPlugin {

    Context context;
    Activity activity;
    private String TAG = "AxwaveSDK";

    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
        context = cordova.getActivity().getApplicationContext();
        activity = cordova.getActivity();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if ("setup".equals(action)) {

            boolean shouldStartSDK = checkPermissions(activity);
            if (shouldStartSDK) {
                setupSdkIfPermissionReady(args.getString(0));
                callbackContext.success("setup sucessfully");
            } else {
                callbackContext.error("User doesn't grant all required premissions. Call setup() again in `onResume` method. ");
            }
            return true;
        }
        if ("setNotificationResources".equals(action)) {
            int icon = 0;
            icon = getAppResource(args.getString(2), "drawable");
            if (icon == 0) icon = getAppResource(args.getString(2), "mipmap");
            if (icon == 0) icon = getAppResource("icon", "mipmap");

            AxwaveSDK.get().setNotificationResources(args.getString(0), args.getString(1), icon);

            return true;
        }

        if ("disable".equals(action)) {
            AxwaveSDK.get().disableDiscovery();
            return true;
        }

        if ("enable".equals(action)) {
            AxwaveSDK.get().enableDiscovery();
            return true;
        }

        if ("isEnabled".equals(action)) {
            callbackContext.success(AxwaveSDK.get().isDiscoveryEnabled() + "");
            return true;
        }

        if ("kill".equals(action)) {
            AxwaveSDK.get().disableDiscovery().stopForegroundNotification();
            return true;
        }

        if ("autostart".equals(action)) {
            AxwaveSDK.get().setAutostart(args.getBoolean(0));
            return true;
        }

        if ("foregroundEnable".equals(action)) {
            AxwaveSDK.get().releaseMicrophonePolicy().enable().getWhitelist().add(context.getPackageName());
            return true;
        }

        if ("foregroundDisable".equals(action)) {
            AxwaveSDK.get().releaseMicrophonePolicy().disable();
            return true;
        }

        if ("foregroundStatus".equals(action)) {
            callbackContext.success(AxwaveSDK.get().releaseMicrophonePolicy().isEnable() + "");
            return true;
        }

        if ("callback".equals(action)) {
            autoCallback = callbackContext;
            return true;
        }

        return true;
    }

    CallbackContext autoCallback = null;

    private void Loggerd(String tag, String msg) {
        if (autoCallback != null) {
            final PluginResult pluginResult = new PluginResult(PluginResult.Status.OK, msg);
            pluginResult.setKeepCallback(true);
            autoCallback.sendPluginResult(pluginResult);
        }
    }

    private void Loggere(String tag, String msg) {
        if (autoCallback != null) {
            final PluginResult pluginResult = new PluginResult(PluginResult.Status.ERROR, msg);
            pluginResult.setKeepCallback(true);
            autoCallback.sendPluginResult(pluginResult);
        }
    }

    class AutoDiscoveryCallback extends DiscoveryReceiver {

        private static final String TAG = "AutoDiscoveryCallback";

        @Override
        public void onReady() {
            Loggerd(TAG, "onReady");
        }

        @Override
        public void onSessionStarted() {
            Loggerd(TAG, "onSessionStarted");
        }

        @Override
        public void onMatch(String matchData) {
            try {
                Loggerd(TAG, "Match: " + matchData);
            } catch (Exception e) {
                Loggere(TAG, "Exception parsing json: " + e);
            }
        }

        @Override
        public void onSendFp() {
            Loggerd(TAG, "onSendFp (" + AxwaveSDK.get().getStats().howManyFpDidSendDuringLastHour() + ")");
        }

        @Override
        public void onSessionEnded() {
            Loggerd(TAG, "onSessionEnded");
        }

        @Override
        public void onFailure(FailureType failureType) {
            Loggere(TAG, "onFailure: " + failureType);
        }

        @Override
        public void onCustomDataResult(boolean b) {
        }

    }

    private int getAppResource(String name, String type) {
        return cordova.getActivity().getResources().getIdentifier(name, type, cordova.getActivity().getPackageName());
    }

    private boolean checkPermissions(Activity activity) {

        if (shouldCheckUsageStats() && android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP && !hasUsageStatsPermission(context)) {
            Toast toast = Toast.makeText(activity, "Click \"Axwave Panelist\" and enable \"Permit usage access\"", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
            toast.show();

            this.cordova.getActivity().startActivity(new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS));
            return false;
        }

        if (isAndroidM()) {
            if (!checkMicrophonePermission(activity)
                    && !checkWriteExternalStoragePermission(activity)) {
                activity.requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        123);
                Log.w(TAG, "Permissions not available, requesting...");
                return false;
            }
            if (!checkMicrophonePermission(activity)) {
                activity.requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO},
                        124);
                Log.w(TAG, "Microphone permission not available, requesting...");
                return false;
            }

            if (!checkWriteExternalStoragePermission(activity)) {
                activity.requestPermissions(
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        125);
                Log.w(TAG, "Write external storage permission not available, requesting...");
                return false;
            }
        }

        return true;
    }

    private void setupSdkIfPermissionReady(String devKey) {

        AxwaveSDK sdk = AxwaveSDK.with(context.getApplicationContext());

        com.axwave.sdk.utils.Utils.setDevKey(context.getApplicationContext(), devKey);

        sdk.setup();

        sdk.registerDiscoveryReceiver(new AutoDiscoveryCallback());
        sdk.enableDiscovery();

    }

    private boolean hasUsageStatsPermission(Context context) {
        AppOpsManager appOps = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);
        if (appOps == null) return false;
        try {
            int mode = appOps.checkOpNoThrow("android:get_usage_stats",
                    android.os.Process.myUid(), context.getPackageName());
            return mode == AppOpsManager.MODE_ALLOWED;
        } catch (Exception exception) {
            Logger.e(TAG, exception);
            return true;
        }
    }

    private boolean shouldCheckUsageStats() {
        return context.getPackageManager().checkPermission(Manifest.permission.GET_TASKS,
                context.getPackageName()) == android.content.pm.PackageManager.PERMISSION_GRANTED;
    }

    public boolean isAndroidKitKat() {
        return android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT;
    }

    public boolean isAndroid5() {
        return android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP;
    }

    public boolean isAndroidM() {
        return android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M;
    }

    private boolean checkMicrophonePermission(Context context) {
        String permission = "android.permission.RECORD_AUDIO";
        int res = context.checkCallingOrSelfPermission(permission);
        return (res == android.content.pm.PackageManager.PERMISSION_GRANTED);
    }

    private boolean checkAccessCoarseLocationPermission(Context context) {
        String permission = "android.permission.ACCESS_COARSE_LOCATION";
        int res = context.checkCallingOrSelfPermission(permission);
        return (res == android.content.pm.PackageManager.PERMISSION_GRANTED);
    }

    private boolean checkWriteExternalStoragePermission(Context context) {
        String permission = "android.permission.WRITE_EXTERNAL_STORAGE";
        int res = context.checkCallingOrSelfPermission(permission);
        return (res == android.content.pm.PackageManager.PERMISSION_GRANTED);
    }

    private String getStringResource(String name) {
        return this.activity.getString(
                this.activity.getResources().getIdentifier(
                        name, "string", this.activity.getPackageName()));
    }

}
